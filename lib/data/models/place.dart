
import 'dart:convert';

Place placeFromJson(String str) => Place.fromJson(json.decode(str));

String placeToJson(Place data) => json.encode(data.toJson());

class Place {
  String placeName;
  String longitude;
  String postCode;
  String latitude;

  Place({
    required this.placeName,
    required this.longitude,
    required this.postCode,
    required this.latitude,
  });

  factory Place.fromJson(Map<String, dynamic> json) => Place(
    placeName: json["place name"],
    longitude: json["longitude"],
    postCode: json["post code"],
    latitude: json["latitude"],
  );

  Map<String, dynamic> toJson() => {
    "place name": placeName,
    "longitude": longitude,
    "post code": postCode,
    "latitude": latitude,
  };
}
