
import 'dart:convert';

Zipcode zipcodeFromJson(String str) => Zipcode.fromJson(json.decode(str));

String zipcodeToJson(Zipcode data) => json.encode(data.toJson());

class Zipcode {
  String postCode;
  String country;
  String countryAbbreviation;
  List<ZipPlace> places;

  Zipcode({
    required this.postCode,
    required this.country,
    required this.countryAbbreviation,
    required this.places,
  });

  factory Zipcode.fromJson(Map<String, dynamic> json) => Zipcode(
    postCode: json["post code"],
    country: json["country"],
    countryAbbreviation: json["country abbreviation"],
    places: List<ZipPlace>.from(json["places"].map((x) => ZipPlace.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "post code": postCode,
    "country": country,
    "country abbreviation": countryAbbreviation,
    "places": List<dynamic>.from(places.map((x) => x.toJson())),
  };
}

class ZipPlace {
  String placeName;
  String longitude;
  String state;
  String stateAbbreviation;
  String latitude;

  ZipPlace({
    required this.placeName,
    required this.longitude,
    required this.state,
    required this.stateAbbreviation,
    required this.latitude,
  });

  factory ZipPlace.fromJson(Map<String, dynamic> json) => ZipPlace(
    placeName: json["place name"],
    longitude: json["longitude"],
    state: json["state"],
    stateAbbreviation: json["state abbreviation"],
    latitude: json["latitude"],
  );

  Map<String, dynamic> toJson() => {
    "place name": placeName,
    "longitude": longitude,
    "state": state,
    "state abbreviation": stateAbbreviation,
    "latitude": latitude,
  };
}
