import 'package:codigos_postales/domain/services/cplogger.dart';
import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';

class LogImpl implements CpLogger {
  @override
  final Logger logger = Logger(
    printer: PrettyPrinter(
        methodCount: 2,
        errorMethodCount: 8,
        lineLength: 120,
        colors: true,
        printEmojis: true,
        printTime: true),
  );

  @override
  void i(dynamic message) {
    logger.i(message);
  }

  @override
  void d(dynamic message) {
    if (kDebugMode) {
      logger.d(message);
    }
  }

  @override
  void e(dynamic message) {
    if (kDebugMode) {
      logger.e(message);
    }
  }
}
