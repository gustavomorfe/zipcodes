

import 'package:codigos_postales/data/models/zipcode.dart';
import 'package:codigos_postales/domain/services/postal_codes_repository.dart';

import '../api/api_impl.dart';

class ZipcodesRepositoryImpl extends PostalCodesRepository {
  final api = ApiImpl();

  @override
  Future<Zipcode> searchLocalizations({required String query}) async {
    final response = await api.fetchZipCodesData(
      query,
    );
   Zipcode responseModel = zipcodeFromJson(response);

   return responseModel;

  }
}
