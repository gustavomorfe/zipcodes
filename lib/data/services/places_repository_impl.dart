import 'dart:convert';

import 'package:codigos_postales/data/api/api_impl.dart';
import 'package:codigos_postales/data/models/place.dart';
import 'package:codigos_postales/domain/services/places_repository.dart';

class PlacesRepositoryImpl extends PlacesRepository {
  final api = ApiImpl();

  @override
  Future<List<Place>> searchPostalCodes({required String query}) async {
    final body = await api.fetchCityData(
      query,
    );
    var bodyJson = json.decode(
      body,
    );

    return (bodyJson['places'] as List)
        .map(
          (
            place,
          ) =>
              Place.fromJson(
            place,
          ),
        )
        .toList();
  }
}
