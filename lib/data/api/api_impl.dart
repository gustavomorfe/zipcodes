import 'package:http/http.dart' as http;

import '../../domain/api/api.dart';
import '../services/logger_impl.dart';

class ApiImpl extends Api {
  final String zipCodesEndpoint = "https://api.zippopotam.us/es";
  final log = LogImpl();

  @override
  Future<String> fetchZipCodesData(String query) async {
    try {
      final encQuery = Uri.encodeComponent(query);
      var response = await http.get(Uri.parse("$zipCodesEndpoint/$encQuery"));

      if (response.statusCode == 200) {
        return response.body;
      } else if (response.statusCode == 404) {
        throw HttpRequestFailure.notFound();
      } else if (response.statusCode >= 500) {
        throw HttpRequestFailure.server();
      }
    } catch (e) {
      return "";
    }
    return "";
  }

  @override
  Future<String> fetchCityData(String query) async {
    try {
      final encQuery = Uri.encodeComponent(query);
      var response =
          await http.get(Uri.parse("$zipCodesEndpoint/ct/$encQuery"));

      if (response.statusCode == 200) {
        return response.body;
      } else if (response.statusCode == 404) {
        throw HttpRequestFailure.notFound();
      } else if (response.statusCode >= 500) {
        throw HttpRequestFailure.server();
      }
    } catch (e) {
      return "";
    }

    return "";
  }
}

class HttpRequestFailure {
  static notFound() {
    throw ("Resource not found");
  }

  static server() {
    throw ("Server not found");
  }
}
