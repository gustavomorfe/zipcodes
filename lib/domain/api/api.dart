abstract class Api {
  Future<String> fetchCityData(String query);

  Future<String> fetchZipCodesData(String query);
}
