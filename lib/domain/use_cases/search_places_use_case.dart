import 'package:codigos_postales/data/services/places_repository_impl.dart';
import 'package:codigos_postales/data/services/zipcodes_repository_impl.dart';

import '../../data/services/logger_impl.dart';
import '../../ui/models/place.dart';

class SearchPlacesUseCase {
  late List<PlaceUI> places;
  PlacesRepositoryImpl placesRepo = PlacesRepositoryImpl();
  ZipcodesRepositoryImpl zipRepo = ZipcodesRepositoryImpl();

  final log = LogImpl();
  Future<List<PlaceUI>> getLocalizations({required String query}) async {
    if (num.tryParse(query)?.isFinite == true) {
      if (query.length == 5) {
        return _getPostCodes(query: query);
      }
    } else {
      return _getPlaces(query: query);
    }
    return places;
  }

  Future<List<PlaceUI>> _getPlaces({required String query}) async {
    if (query.isNotEmpty) {
      places = [];
      try {
        await placesRepo.searchPostalCodes(query: query).then((placesData) {
          places = placesData
              .map(
                (p) => PlaceUI(
                  placeName: p.placeName,
                  longitude: p.longitude,
                  postCode: p.postCode,
                  latitude: p.latitude,
                ),
              )
              .toList();
          return places;
        }).onError(
          (error, stackTrace) {
            log.e("Error: $error. Stack: $stackTrace");
            return places;
          },
        );
      } catch (e) {
        log.e("Error: $e");
        return places;
      }
    }
    return places;
  }

  Future<List<PlaceUI>> _getPostCodes({required String query}) async {
    if (query.isNotEmpty) {
      places = [];
      try {
        await zipRepo.searchLocalizations(query: query).then(
          (z) {
            places.add(
              PlaceUI(
                postCode: z.postCode,
                longitude: z.places[0].longitude,
                latitude: z.places[0].latitude,
                placeName: z.places[0].placeName,
              ),
            );
          },
        ).onError((error, stackTrace) {
          log.e(" Error at _getPostCodes: $error. Stack: $stackTrace");
        });
      } catch (e) {
        log.e("Error: $e");
        return places;
      }
    }
    return places;
  }
}
