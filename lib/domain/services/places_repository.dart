// ignore: unused_import
import 'package:codigos_postales/data/models/zipcode.dart';

import '../../data/models/place.dart';

abstract class PlacesRepository {
  Future<List<Place>> searchPostalCodes({
    required String query,
  });
}
