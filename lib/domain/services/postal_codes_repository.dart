import '../../data/models/zipcode.dart';

abstract class PostalCodesRepository {
  Future<Zipcode> searchLocalizations({
    required String query,
  });
}
