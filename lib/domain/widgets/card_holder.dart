import 'package:flutter/material.dart';

abstract class CardHolder {
  Widget build();
}