import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import 'ui/screens/zipcodes/zipcodes_home.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  HttpOverrides.global = MyHttpOverrides();
  await EasyLocalization.ensureInitialized();
  runApp(
    EasyLocalization(
      supportedLocales: const [
        Locale('en'),
        Locale('de'),
        Locale('es'),
        Locale('ca'),
      ],
      path: 'assets/l10n',
      fallbackLocale: const Locale('en'),
      child: const MyApp(),
    ),
  );
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      title: "Zip Codes",
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          color: Colors.orange,
          titleTextStyle: TextStyle(
            color: Colors.black87,
            fontSize: 20,
          ),
          iconTheme: IconThemeData(
            color: Colors.black87,
          ), // Color de fondo de la barra de navegación
        ),
        scaffoldBackgroundColor: Colors.white,
        textTheme:  const TextTheme(
          bodyLarge: TextStyle(color: Colors.black87),
          bodyMedium: TextStyle(color: Colors.black87),
          titleLarge: TextStyle(color: Colors.black),
        ),
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.orangeAccent,
        ).copyWith(background: Colors.white),
      ),
      home: const ZipCodesHome(),
    );
  }
}
