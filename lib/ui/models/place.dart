
import 'dart:convert';

PlaceUI placeFromJson(String str) => PlaceUI.fromJson(json.decode(str));

String placeToJson(PlaceUI data) => json.encode(data.toJson());

class PlaceUI {
  String placeName;
  String longitude;
  String postCode;
  String latitude;

  PlaceUI({
    required this.placeName,
    required this.longitude,
    required this.postCode,
    required this.latitude,
  });

  factory PlaceUI.fromJson(Map<String, dynamic> json) => PlaceUI(
    placeName: json["place name"],
    longitude: json["longitude"],
    postCode: json["post code"],
    latitude: json["latitude"],
  );

  Map<String, dynamic> toJson() => {
    "place name": placeName,
    "longitude": longitude,
    "post code": postCode,
    "latitude": latitude,
  };
}
