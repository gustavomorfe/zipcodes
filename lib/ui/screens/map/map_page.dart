import 'package:codigos_postales/ui/models/place.dart';
import 'package:codigos_postales/ui/shared/widgets/localization_card_holder.dart';
import 'package:codigos_postales/ui/shared/widgets/localization_card_nonav.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_location_marker/flutter_map_location_marker.dart';
import 'package:latlong2/latlong.dart';

class MapPage extends StatefulWidget {
  const MapPage({super.key, required this.place});

  final PlaceUI place;

  @override
  State<MapPage> createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  late double longitude;
  late double latitude;
  late LocationMarkerPosition currentPosition;
  late LocationMarkerHeading currentHeading;

  initUI() async {
    longitude = double.parse(widget.place.longitude);
    latitude = double.parse(widget.place.latitude);
    currentPosition = LocationMarkerPosition(
      latitude: latitude,
      longitude: longitude,
      accuracy: pi * 0.2,
    );
    currentHeading = LocationMarkerHeading(
      heading: 0,
      accuracy: pi * 0.2,
    );
  }

  @override
  initState() {
    super.initState();
    initUI();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 16),
                Padding(
                  padding: EdgeInsets.zero,
                  child: GestureDetector(
                      onTap: () => Navigator.pop(context),
                      child: const Icon(
                        Icons.close,
                        size: 24,
                        semanticLabel: "close button",
                      )),
                ),
                const SizedBox(height: 32),
                Card(
                  elevation: 20,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                        16.0,
                      ),
                      side: const BorderSide(
                        color: Colors.black87,
                        width: 5,
                        strokeAlign: 1,
                      )),
                  child: Container(
                    height: 400,
                    width: double.infinity,
                    color: Colors.white70,
                    child: FlutterMap(
                      options: MapOptions(
                        initialCenter: LatLng(latitude, longitude),
                        minZoom: 7,
                        maxZoom: 19,
                        initialZoom: 11.2,
                      ),
                      children: [
                        TileLayer(
                          minNativeZoom: 0,
                          urlTemplate:
                              'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                          userAgentPackageName:
                              'es.gustavomoreno.codigos_postales',
                        ),
                        LocationMarkerLayer(
                          position: currentPosition,
                          heading: null,
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 36,
                ),
                LocalizationCardHolder(
                  card: CardNonav(
                    localization: widget.place,
                    context: context,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
