import 'package:codigos_postales/ui/screens/zipcodes/widgets/search_bar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';

class ZipCodesHome extends StatelessWidget {
  const ZipCodesHome({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Center(
            child: Padding(
              padding: const EdgeInsets.only(
                right: 56.0,
              ),
              child: const Text(
                'search',
              ).tr(),
            ),
          ),
        ),
        body: const KeyboardDismisser(
          child: NiceSearchBar(),
        ),
        drawer: const Drawer(),
      ),
    );
  }
}
