import 'package:codigos_postales/domain/use_cases/search_places_use_case.dart';
import 'package:codigos_postales/ui/shared/widgets/localization_card_holder.dart';
import 'package:codigos_postales/ui/shared/widgets/localization_card_nav.dart';
import 'package:easy_debounce/easy_debounce.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../../models/place.dart';

class NiceSearchBar extends StatefulWidget {
  const NiceSearchBar({super.key});

  @override
  State<NiceSearchBar> createState() => _NiceSearchBarState();
}

class _NiceSearchBarState extends State<NiceSearchBar> {
  String searchText = '';
  final _controller = TextEditingController();
  SearchPlacesUseCase search = SearchPlacesUseCase();

  late Future<List<PlaceUI>> places;

  Future<List<PlaceUI>> fetchData(String query) async {
    try {
      return await search.getLocalizations(
        query: query,
      );
    } catch (e) {
      return [];
    }
  }

  @override
  void initState() {
    super.initState();
    places = Future.value([]);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        const SizedBox(height: 16),
        Padding(
          padding: const EdgeInsets.all(
            16.0,
          ),
          child: TextField(
            controller: _controller,
            maxLines: 1,
            keyboardType: TextInputType.name,
            decoration: InputDecoration(
              prefixIcon: const Icon(
                Icons.search,
              ),
              hintText: tr(
                'searchHint',
              ),
              suffixIcon: (_controller.text.isNotEmpty)
                  ? IconButton(
                      onPressed: () {
                        setState(() {
                          _controller.clear();
                        });
                      },
                      icon: const Icon(
                        Icons.close,
                      ),
                    )
                  : const SizedBox(),
              filled: true,
            ),
            onChanged: (query) {
              EasyDebounce.debounce(
                'searchQueryDebouncer',
                const Duration(milliseconds: 150),
                () => setState(
                  () {
                    places = (query.isNotEmpty)
                        ? fetchData(query)
                        : Future.value([]);
                  },
                ),
              );
            },
          ),
        ),
        FutureBuilder<List<PlaceUI>>(
          future: places,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Padding(
                padding: EdgeInsets.all(32.0),
                child: Center(child: CircularProgressIndicator()),
              );
            } else if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
              return Container();
            } else {
              return Expanded(
                child: GridView.builder(
                  physics: const ScrollPhysics(),
                  // ignore: prefer_const_constructors
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    mainAxisSpacing: 12.0, // spacing between rows
                    crossAxisSpacing: 5.0,
                    maxCrossAxisExtent: 200, // spacing between columns
                  ),
                  padding: const EdgeInsets.all(8.0),
                  // padding around the grid
                  itemCount: snapshot.data!.length,
                  // total number of items
                  itemBuilder: (context, index) {
                    return LocalizationCardHolder(
                      card: CardNav(
                        localization: snapshot.data![index],
                        context: context,
                      ),
                    );
                  },
                ),
              );
            }
          },
        )
      ],
    );
  }
}
