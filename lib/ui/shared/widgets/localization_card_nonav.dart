import 'package:codigos_postales/domain/widgets/card_holder.dart';
import 'package:codigos_postales/ui/models/place.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class CardNonav implements CardHolder {
  final PlaceUI localization;
  final BuildContext context;

  CardNonav({required this.localization, required this.context});

  @override
  Widget build() {
    return Card(
      elevation: 1,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
        side: const BorderSide(
          color: Colors.black26,
          width: 1,
        ),
      ),
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              localization.placeName,
              style: const TextStyle(fontSize: 22),
            ),
            Text(
              localization.postCode,
              style: const TextStyle(fontSize: 21),
            ),
            Container(
              padding: EdgeInsets.zero,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 12,
                  ),
                  Text(
                    "${tr('latitude')}:  "
                    "${localization.latitude}",
                    style: const TextStyle(fontSize: 11),
                  ),
                  Text(
                    "${tr('longitude')}:  "
                    "${localization.longitude}",
                    style: const TextStyle(fontSize: 11),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
