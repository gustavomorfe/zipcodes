import 'package:codigos_postales/domain/widgets/card_holder.dart';
import 'package:flutter/material.dart';


class LocalizationCardHolder extends StatelessWidget {

  final CardHolder card;

  const LocalizationCardHolder({super.key, required this.card});

  @override
  Widget build(BuildContext context) {
    return card.build();
  }
}
