import 'package:codigos_postales/domain/widgets/card_holder.dart';
import 'package:codigos_postales/ui/models/place.dart';
import 'package:codigos_postales/ui/screens/map/map_page.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class CardNav implements CardHolder {
  final PlaceUI localization;
  final BuildContext context;

  CardNav({required this.localization, required this.context});

  @override
  Widget build() {
    return InkWell(
      onTap: () {
        if (localization.longitude.isEmpty || localization.latitude.isEmpty) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(
                tr("latLonNotAvailable"),
              ),
            ),
          );
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => MapPage(
                place: localization,
              ),
            ),
          );
        }
      },
      child: Card(
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
          side: const BorderSide(
            color: Colors.black38,
            width: 1,
            strokeAlign: 1,
          ),
        ),
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                localization.placeName,
                style: const TextStyle(fontSize: 21),
              ),
              Text(
                "${tr('postalCodeAKA')}:  ${localization.postCode}",
                style: const TextStyle(fontSize: 17),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
