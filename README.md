# Zip Codes
Zip Codes Locator es una herramienta especializada en encontrar la localización de códigos postales y para encontrar el código postal de una localidad. Nuestra aplicación proporciona una solución rápida y precisa para identificar la ubicación de códigos postales de pueblos en un mapa detallado. Optimiza tus procesos al acceder fácilmente a la información postal sin la necesidad de realizar búsquedas complicadas

## Cómo Clonar e Iniciar la Aplicación
Para obtener y ejecutar la aplicación en tu entorno local, sigue estos pasos:
### Clonar el Repositorio:
```sh
git clone https://gitlab.com/gustavomorfe/zipcodes.git
```
### Navegar al Directorio del Proyecto:
```sh
cd zipcodes
```
### Instalar Dependencias:
```sh
flutter pub get
```
### Ejecutar la Aplicación:
```sh
flutter run
```


## Dependecias

Flutter Runner necesita [Flutter](https://flutter.dev/) >=3.1.5 <4.0.0 para compilarse
